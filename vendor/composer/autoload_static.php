<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2db302bf24c09088f2c2b99e04020275
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Cestolino\\GoogleAPI\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Cestolino\\GoogleAPI\\' => 
        array (
            0 => __DIR__ . '/..' . '/kubicek_o/google-map-api/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2db302bf24c09088f2c2b99e04020275::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2db302bf24c09088f2c2b99e04020275::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
